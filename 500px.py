#!/home/pi/selenium_test/bin/python

# FORMAT python 500px.py 0 50
# arg 1: mods [ 0 - 2 ]
# arg 2: max likes [100]
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from dotenv import load_dotenv

from datetime import datetime
import os
import sys
import time
import random
import pickle
import io
import mail
import pandas as pd

load_dotenv()
options = Options()
# options.headless = True
# options.add_argument("--no-sandbox")
# driver = webdriver.Chrome(executable_path="/usr/bin/chromedriver", options=options)
driver = webdriver.Chrome(chrome_options=options)
driver.implicitly_wait(30)


commentsArray = [
    "Woow i'm amazed.I really like your photo.",
    "Wow i'm amazed.I like it.",
    "I just enjoy your photos, well done.",
    "I really like your photo.",
    "Love this one.",
    "The colors, composition just a joy.",
    "My words are less to describe this picture.",
    "Impressive photo, well done.",
    "Such a charming photo.",
    "Great choice of focal point.",
    "Great capture.",
    "Amazing composition!",
    "What a lovely, dramatic shot!",
    "Amazing!",
    "Nice shot!",
    "I love your profile!",
    "Wow.",
    "Just incredible.",
    "I enjoy your art, keep it going." "Amazing.",
    "Love your posts.",
    "Looks awesome.",
    "Getting inspired by you.",
    "So cool.",
    "I can feel your passion.",
]

config = {
    "base_url": "https://500px.com",
    "login_url": "https://500px.com/login",
    "popular": "https://500px.com/popular",
    "upcoming": "https://500px.com/upcoming",
    "fresh": "https://500px.com/fresh",
    "maxLike": 50,
    "maxComment": 20,
}
pages = ["/popular", "/upcoming", "/fresh"]

activity = pd.DataFrame()
authors = []
photoNames = []
likes = []
comments = []
photoID = []

photos_liked = 0
photos_commented = 0

photos_liked = 0
photos_commented = 0


def addDataToDataFrame(author, photoName, liked, comment, ID):
    global authors
    global photoNames
    global likes
    global comments
    global photoID
    authors.append(author)
    photoNames.append(photoName)
    likes.append(liked)
    comments.append(comment)
    photoID.append(str(ID))


def saveCSV():
    global activity
    global authors
    global photoNames
    global likes
    global comments
    global photoID
    activity["author"] = authors
    activity["photo"] = photoNames
    activity["photo_url"] = photoID
    activity["like"] = likes
    activity["comment"] = comments
    activity.to_csv("activity.csv", index=False, header=True)


# save session
def save_cookies():
    pickle.dump(driver.get_cookies(), open("cookies_500px.pkl", "wb"))


# check for cookies
def get_cookies():
    try:
        cookies = pickle.load(open("cookies_500px.pkl", "rb"))
        if cookies:
            for cookie in cookies:
                driver.add_cookie(cookie)
            return True
    except FileNotFoundError as ex:
        print("Cookie not found. Preceding to log in.")
    return False


# Check if account if logged id [ if 'Upload' button is present it's logged in]
def checkLogin():
    try:
        upload_button = WebDriverWait(driver, 15).until(
            EC.presence_of_element_located(
                (By.XPATH, "//span[contains(text(), 'Upload')]/parent::button")
            )
        )
        if upload_button:
            print("Successful Login!")
            return True
    except TimeoutException as ex:
        print("Not Logged in.")
    return False


# Waits random human-like time
def waitRandomTime():
    waitTime = random.uniform(1.2, 5.5)
    # print("Waiting " + str(waitTime) + "sec...")
    time.sleep(waitTime)


# Writes string to file
def writeToFile(data):
    with io.open("activity.txt", "a", encoding="utf-8") as outfile:
        outfile.write(data + "\n")
        outfile.close()


# Chance of doing something in %
def rollDice(percente):
    dice = random.uniform(0, 101)
    if dice > 100 - percente:
        return True
    return False


# Scroll page to bottom random number of times
def scrollDown():
    for i in range(2):
        html = driver.find_element_by_tag_name("html")
        html.send_keys(Keys.PAGE_DOWN)


# Look for new photos
def findNewPhotos():
    photo_divs = []
    minFindPhotos = 10
    while len(photo_divs) < minFindPhotos:
        photo_divs = driver.find_elements_by_xpath(
            "//div[contains(@class,normalize-space('photo_thumbnail')) and descendant::*//div[contains(@class, 'like-button') and not(.//a[contains(@class, 'hearted')])] ]"
        )
        if len(photo_divs) < minFindPhotos:
            scrollDown()
            time.sleep(2)
    print(f"Found {len(photo_divs)} new photos!")
    return photo_divs


# simulates a mistake in commenting. adding or leaving a character
def comment_mistake(com):
    new_comment = com
    num_of_chars = len(new_comment)
    # Make spelling mistake [forgot-character]
    make_mistake = rollDice(50)

    if make_mistake == True:
        print("Making comment mistake 1.")
        if num_of_chars > 10:
            position = random.randint(1, num_of_chars - 1)
            new_comment = new_comment[:position] + new_comment[position + 1 :]

    # Make spelling mistake[add one too many characters]
    if make_mistake == False:
        print("Making comment mistake 2.")
        if num_of_chars > 3:
            position = random.randint(1, num_of_chars - 1)
            new_comment = (
                new_comment[:position]
                + new_comment[position - 1]
                + new_comment[position + 1 :]
            )
    return new_comment


# like photo
def like_photo():
    try:
        like_button = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located(
                (By.XPATH, "//div[@aria-label='Unlike photo']/div")
            )
        )
        like_button.click()
        return True
    except TimeoutException as ex:
        print("Already liked this photo!")
    return False


# Generate a comment from comment list
def generateComment():
    num_of_comments = len(commentsArray) - 1
    comment = commentsArray[random.randint(0, num_of_comments)]

    make_mistake = rollDice(20)
    if make_mistake == True:
        comment = comment_mistake(comment)

    return comment


# comment photo
def comment_photo():
    try:
        comment_input = WebDriverWait(driver, 6).until(
            EC.presence_of_element_located((By.XPATH, "//textarea[@name='comment']"))
        )
        comment = generateComment()
        comment_input.send_keys(comment)
        post_button = driver.find_element_by_xpath(
            "//span[text()='Post']/parent::button"
        )
        waitRandomTime()
        post_button.click()
        return True, comment
    except TimeoutException as ex:
        print("Already commented this photo!")
    return False, ""


def likeAndCommentPhoto(link):
    global photos_commented
    global photos_liked

    liked = False
    commented = False
    comment = ""
    pref = ""
    driver.execute_script("window.open('" + link + "','_blank');")
    driver.switch_to.window(driver.window_handles[1])
    waitRandomTime()
    # get info
    authorDiv = WebDriverWait(driver, 5).until(
        EC.presence_of_element_located(
            (By.XPATH, "//h3[contains(@class, 'HeadingMedium')]/parent::div")
        )
    )
    nameOfPhoto = authorDiv.find_element_by_tag_name("h3").text
    authorName = authorDiv.find_element_by_tag_name("a").text
    photoURL = link.split("/")[4]
    info = nameOfPhoto + " by " + authorName[:-4]

    # like the photo
    liked = like_photo()
    if liked == True:
        photos_liked += 1
        pref += "L"

    waitRandomTime()

    # comment on the photo
    willComment = rollDice(25)
    if willComment == True:
        commented, comment = comment_photo()
        if commented == True:
            photos_commented += 1
            pref += "C"

    # Write to file info
    print(f"[{photos_liked}]{pref} \t| {info}")
    # if liked == True:
    #     data = f"{pref}|{nameOfPhoto}|{authorName[:-4]}"
    #     writeToFile(data)

    addDataToDataFrame(authorName, nameOfPhoto, liked, comment, photoURL)
    waitRandomTime()

    # close window
    driver.close()
    driver.switch_to.window(driver.window_handles[0])


# Send mail with data of interactions
def sendData(start, end):
    global photos_liked
    global photos_commented

    year = end.strftime("%Y")
    month = end.strftime("%m")
    day = end.strftime("%d")
    start_time = start.strftime("%H:%M")
    end_time = end.strftime("%H:%M")

    head = f"500pxBOT {month} - {day}"
    body = f"Year: {year} \n Month: {month} \n Day: {day}\n Start: {start_time} \n End: {end_time}\n Liked: {photos_liked} \n Commented: {photos_commented}"
    testmailsender = mail.MailSender(
        os.environ.get("SMTP_EMAIL"),
        os.environ.get("SMTP_PASS"),
        os.environ.get("RCPT"),
    )
    testmailsender.connect()
    testmailsender.set_message(head, body)
    testmailsender.send()


# Login
def login():
    driver.get(config["login_url"])

    # If logged in, get cookies
    cookies = get_cookies()

    if cookies == False:
        # email - usesrname input
        email_input = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "emailOrUsername"))
        )
        email_input.clear()
        email_input.send_keys(os.environ.get("EMAIL"))
        # password input
        email_input = driver.find_element(By.ID, "password")
        email_input.clear()
        email_input.send_keys(os.environ.get("PASSWORD"))
        # submit-button click
        submit_button = driver.find_element_by_xpath(
            "//*[contains(text(), 'Log in')]/parent::button"
        )
        submit_button.click()
        print("Sending login data...")
        # check if successful login
        loggedIn = checkLogin()
        if loggedIn == True:
            save_cookies()
    else:
        driver.refresh()
        loggedIn = checkLogin()
        if loggedIn == False:
            print("Bad cookie. Bad login.")


# Proces of liking and commenting
def like_discover(maxLike):
    global photos_liked

    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, "photo_thumbnail"))
    )
    time.sleep(3)
    # Find all photos that are not liked
    while photos_liked < maxLike:
        photo_divs = []
        num_of_photos = len(photo_divs)

        if num_of_photos == 0:
            photo_divs = findNewPhotos()
            num_of_photos = len(photo_divs)

        if num_of_photos > 0:
            waitRandomTime()
            for div in photo_divs:
                photo = div.find_element_by_class_name("photo_link")
                link = photo.get_attribute("href")
                likeAndCommentPhoto(link)
                waitRandomTime()
                if photos_liked >= maxLike:
                    break
        print(f"Currently liked: {photos_liked}")
        driver.refresh()
        time.sleep(3)


if __name__ == "__main__":
    start_at = datetime.now()
    url = config["popular"]
    maxLike = config["maxLike"]

    if len(sys.argv) > 1:
        url = config["base_url"] + pages[int(sys.argv[1])]
        if len(sys.argv) > 2:
            maxLike = int(sys.argv[2])
        else:
            maxLike = config["maxLike"]

    print(f"Page: {url}")
    print(f"Max like: {maxLike}")
    print("Start at: " + start_at.strftime("%d-%m-%Y %H:%M"))
    # writeToFile("-------------------Started at: " + start_at.strftime("%d-%m-%Y %H:%M"))

    login()
    driver.get(url)
    like_discover(maxLike)
    waitRandomTime()
    end_at = datetime.now()
    sendData(start_at, end_at)
    saveCSV()
    driver.quit()