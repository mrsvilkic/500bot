This is bot made with Python Selenium
Purpose is strictly educational. It engages with 500px photos.
Likes and comments.

Instructions:

Install required libraries

    $pip install selenium
    $pip install python-dotenv

Requires Chromium and Chromedriver.
Login details are stored in .env

    $touch .env
    $nano .env

.env

    EMAIL="example@example.com"
    PASSWORD="password"
    SMTP_EMAIL="email-that-will-send-you-email@gmail.com"
    SMTP_PASS="password-to-that-email"
    RCPT="mail-that-will-receive-summary"

To run the script, you can type

    $python 500px.py

To run one of custome mods run

    $python 500px.py <Mod> <max-likes>
    $python 500px.py 1 40
